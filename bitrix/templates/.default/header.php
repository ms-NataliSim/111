<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<html>
<head>
<?$APPLICATION->ShowHead();?>
<title><?$APPLICATION->ShowTitle()?></title>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.7.min.js"></script>
  <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/tms-0.4.1.js"></script>
  <!-- demo -->
  <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/demo.js"></script>
  <!-- end demo -->  
  <script src="<?=SITE_TEMPLATE_PATH?>/js/highlight.js"></script>
  <script src="<?=SITE_TEMPLATE_PATH?>/js/html5.js"></script>
  <script src="<?=SITE_TEMPLATE_PATH?>/js/html-xml.js"></script>
  <script src="<?=SITE_TEMPLATE_PATH?>/js/css.js"></script>
  <script src="<?=SITE_TEMPLATE_PATH?>/js/javascript.js"></script>
  
  <script>
    hljs.tabReplace = '    ';
    hljs.initHighlightingOnLoad();
  </script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF">

<?$APPLICATION->ShowPanel()?>

<?if($USER->IsAdmin()):?>

<div style="border:red solid 1px">
	<form action="/bitrix/admin/site_edit.php" method="GET">
		<input type="hidden" name="LID" value="<?=SITE_ID?>" />
		<p><font style="color:red"><?echo GetMessage("DEF_TEMPLATE_NF")?> </font></p>
		<input type="submit" name="set_template" value="<?echo GetMessage("DEF_TEMPLATE_NF_SET")?>" />
	</form>
</div>

<?endif?>