<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Супер слайдер</title>
  <meta charset="utf-8">  
	<link rel="stylesheet" href="style.css" type="text/css" media="all">
  <? 
  $APPLICATION->AddHeadScript('/js/jquery-1.7.min.js');
  $APPLICATION->AddHeadScript('/js/jquery.easing.1.3.js');
  $APPLICATION->AddHeadScript('/js/tms-0.4.1.js');
  $APPLICATION->AddHeadScript('/js/demo.js');
  $APPLICATION->AddHeadScript('/js/highlight.js');
  $APPLICATION->AddHeadScript('/js/html5.js');
  $APPLICATION->AddHeadScript('/js/html-xml.js');
  $APPLICATION->AddHeadScript('/js/css.js');
  $APPLICATION->AddHeadScript('/js/javascript.js');?>
  
  <script>
    hljs.tabReplace = '    ';
    hljs.initHighlightingOnLoad();
  </script>
  
  <!--[if lt IE 7]>
  	<link rel="stylesheet" href="css/ie/ie6.css" type="text/css" media="screen">    
  <![endif]-->
  <!--[if lt IE 9]>
  	<script type="text/javascript" src="js/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">    
  <![endif]-->
</head>

<body>
	<div id="main">
						<div id="slide">			  	
							<!-- slider -->
							<div class="slider">
								<ul class="items">
								<?foreach($arResult["ITEMS"] as $arItem):?>
<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
									<li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
	<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
	<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img
	class="preview_picture"
	border="0"
	src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
	width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
	height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
	alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
	title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
	style="float:left"
	/></a>
	
	<?endif;?>
	<?endif;?>
	<div class="banner"><span><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><b><?echo $arItem["NAME"]?></b></a><br /></span></div> 
		</li>
	<?endforeach;?>
									
								</ul>
							</div>	
							<a href="#" class="prev">&lt;</a> <a href="#" class="play"><em>stop</em><span>play</span></a> <a href="#" class="next">&gt;</a>
							<!-- slider end -->	
						</div>
	</div>
</body>
</html>
