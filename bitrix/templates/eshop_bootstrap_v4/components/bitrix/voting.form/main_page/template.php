<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die(); ?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>hashslider - плагин слайдшоу | pcvector.net</title>
	<link rel="shortcut icon" href="/favicon.ico" />

	<!--get jquery-->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
	<!--get the hashslider script (minified)-->
	<script type="text/javascript" src="js/hashslider_minified.js"></script>

	<!--get the css-->
	<link href="hashslider_example.css" rel="stylesheet" type="text/css" />
	
</head>

<body>

	<div id="slider">
		<div class="voting-form-box">
			<form action="<?= POST_FORM_ACTION_URI ?>" method="post" id="art" name="art" onsubmit="return false;">
				<input type="hidden" name="vote" value="Y">
				<input type="hidden" name="PUBLIC_VOTE_ID" value="<?= $arResult["VOTE"]["ID"] ?>">
				<input type="hidden" name="VOTE_ID" value="<?= $arResult["VOTE"]["ID"] ?>">
				<?= bitrix_sessid_post() ?>
				<ul>
					<li>
						<? foreach ($arResult["QUESTIONS"] as $arQuestion): ?>
							<? $id = $arQuestion["ID"]; ?>
							<? if ($id == 1): ?>
								<? if ($arQuestion["IMAGE"] !== false): ?>
									<img src="<?= $arQuestion["IMAGE"]["SRC"] ?>" width="30" height="30" />
								<? endif ?>
								<b>
									<?= $arQuestion["QUESTION"] ?>
									<? if ($arQuestion["REQUIRED"] == "Y") {
										echo "<span class='starrequired'>*</span>";
									} ?>
								</b><br />

								<? foreach ($arQuestion["ANSWERS"] as $arAnswer): ?>
									<?
									switch ($arAnswer["FIELD_TYPE"]):
										case 0: //radio
											$value = (isset($_REQUEST['vote_radio_1' . $arAnswer["QUESTION_ID"]]) &&
												$_REQUEST['vote_radio_1' . $arAnswer["QUESTION_ID"]] == $arAnswer["ID"]) ? 'checked="checked"' : '';
											break;

									endswitch;
									?>
									<? switch ($arAnswer["FIELD_TYPE"]):
										case 0: //radio?>
											<span class="form-control">
												<label><input <?= $value ?> type="radio" name="vote_radio_1"
														value="<?= $arAnswer["ID"] ?>" <?= $arAnswer["~FIELD_PARAM"] ?> />&nbsp;
													<?= $arAnswer["MESSAGE"] ?>
												</label>
												<br />
											</span>
											<? break ?>

									<? endswitch ?>

								<? endforeach ?>

							<? endif ?>

						<? endforeach ?>
					</li>
					<li>
						<? foreach ($arResult["QUESTIONS"] as $arQuestion): ?>
							<? $id = $arQuestion["ID"]; ?>
							<? if ($id == 2): ?>
								<? if ($arQuestion["IMAGE"] !== false): ?>
									<img src="<?= $arQuestion["IMAGE"]["SRC"] ?>" width="30" height="30" />
								<? endif ?>
								<b>
									<?= $arQuestion["QUESTION"] ?>
									<? if ($arQuestion["REQUIRED"] == "Y") {
										echo "<span class='starrequired'>*</span>";
									} ?>
								</b><br />

								<? foreach ($arQuestion["ANSWERS"] as $arAnswer): ?>
									<?
									switch ($arAnswer["FIELD_TYPE"]):
										case 0: //radio
											$value = (isset($_REQUEST['vote_radio_2' . $arAnswer["QUESTION_ID"]]) &&
												$_REQUEST['vote_radio_2' . $arAnswer["QUESTION_ID"]] == $arAnswer["ID"]) ? 'checked="checked"' : '';
											break;

									endswitch;
									?>
									<? switch ($arAnswer["FIELD_TYPE"]):
										case 0: //radio?>
											<span class="form-control">
												<label><input <?= $value ?> type="radio" name="vote_radio_2"
														value="<?= $arAnswer["ID"] ?>" <?= $arAnswer["~FIELD_PARAM"] ?> />&nbsp;
													<?= $arAnswer["MESSAGE"] ?>
												</label>
												<br />
											</span>
											<? break ?>

									<? endswitch ?>

								<? endforeach ?>

							<? endif ?>

						<? endforeach ?>
					</li>
					<li>
						<? foreach ($arResult["QUESTIONS"] as $arQuestion): ?>
							<? $id = $arQuestion["ID"]; ?>
							<? if ($id == 3): ?>
								<? if ($arQuestion["IMAGE"] !== false): ?>
									<img src="<?= $arQuestion["IMAGE"]["SRC"] ?>" width="30" height="30" />
								<? endif ?>
								<b>
									<?= $arQuestion["QUESTION"] ?>
									<? if ($arQuestion["REQUIRED"] == "Y") {
										echo "<span class='starrequired'>*</span>";
									} ?>
								</b><br />

								<? foreach ($arQuestion["ANSWERS"] as $arAnswer): ?>
									<?
									switch ($arAnswer["FIELD_TYPE"]):
										case 0: //radio
											$value = (isset($_REQUEST['vote_radio_3' . $arAnswer["QUESTION_ID"]]) &&
												$_REQUEST['vote_radio_3' . $arAnswer["QUESTION_ID"]] == $arAnswer["ID"]) ? 'checked="checked"' : '';
											break;

									endswitch;
									?>
									<? switch ($arAnswer["FIELD_TYPE"]):
										case 0: //radio?>
											<span class="form-control">
												<label><input <?= $value ?> type="radio" name="vote_radio_3"
														value="<?= $arAnswer["ID"] ?>" <?= $arAnswer["~FIELD_PARAM"] ?> />&nbsp;
													<?= $arAnswer["MESSAGE"] ?>

												</label>
												<br />
											</span>
											<? break ?>

									<? endswitch ?>

								<? endforeach ?>
							<? endif ?>

						<? endforeach ?>
					</li>
					<li>
						<button class="btn"><input type="submit" id="open" value="Голосовать"
								class="btn btn-gradient btn-glow" />
						</button>
						<dialog>
							<p>

								Благодарим за участие в голосовании! </p>
							<p><a href=https://vlars.ru /><img
									src=https://vlars.ru/wp-content/uploads/2022/01/1msi.gif></a></p>

							<p>
								Ваш голос получен ☑️. После закрытия окна вы будете перенаправлены на
								страницу результатов голосования.

							</p>
							<p>
								<button class="btn"><input type="submit" id="close" value="Закрыть окно"
										class="btn btn-gradient btn-glow" /></button>
								<!--<button id="close">Закрыть окно </button>-->

							</p>
						</dialog>

						<script>
							var dialog = document.querySelector('dialog')
							const form = document.getElementById('art');
							form.addEventListener('submit', function (event) {
								event.preventDefault();
								if (($('input[name="vote_radio_1"]').is(':checked'))&&($('input[name="vote_radio_2"]').is(':checked'))&&($('input[name="vote_radio_3"]').is(':checked'))) {
									// выводим модальное окно
									document.querySelector('#open').onclick = function () {
										dialog.showModal()
									}
									// скрываем окно
									document.querySelector('#close').onclick = function () {
										dialog.close()
										form.submit()
									}
								}
								else {
									alert('⚠️Обязательный вопрос без ответа! Укажите ответ для вопросо со звездочкой *');
								}
							});

						</script>
					</li>
				</ul>
			</form>
		</div>
	</div>

	<!--fancystuff - тень-->
	<div style="position: relative; width: 660px; height: 38px; margin: auto auto; z-index: 10;"><img src="shadow.png"
			alt="тень" /></div>

	<!--the navigation buttons-->
	<div id="buttons">
		<div id="left"> Назад </div>
		<div id="right"> Вперед </div>
	</div>
	<div style="clear: both;"></div>

	<!--the number navigation-->
	<ul id="numbers">
		<li></li>
	</ul>
	<!-- pcvector.net -->
	<script type="text/javascript" src="/js/pcvector.js"></script>
	<!-- /pcvector.net -->
	<style>
	dialog {
			margin: auto;
			background: #FFFFFF;
			border: 3px solid;
			border-color: #016eda;
			font-family: Monotype Corsiva;

			font-size: 25px;
			font-weight: bold;
			text-align: center;
		}
</style>
</body>

</html>
