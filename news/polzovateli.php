<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Пользователи");
$APPLICATION->SetTitle("Пользователи");
?>
<table border="2" class="allusers">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Таблицы</title>
  <style type="text/css">
   TABLE {
    border-collapse: collapse; /* Убираем двойные линии между ячейками */
    width: 100%; /* Ширина таблицы */
   }
   TH { 
    background: #fc0; /* Цвет фона ячейки */
    text-align: left; /* Выравнивание по левому краю */
   }
   TD {
    background: #fff; /* Цвет фона ячеек */
    text-align: center; /* Выравнивание по центру */
   }
   TH, TD {
    border: 1px solid black; /* Параметры рамки */
    padding: 4px; /* Поля вокруг текста */
   }
  </style>
<thead>
<tr>
	<th>
	</th>
	<th>
		Пользователь
	</th>
	<th>
		Имя
	</th>
	
</tr>
</thead>
<?
$filter = Array("GROUPS_ID"=> Array(8));
$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter);
while($arItem = $rsUsers->GetNext()){
    $FotoUser = CFile::ShowImage($arItem["PERSONAL_PHOTO"], 100, 100, "border=0", "", true);
    echo "<tr><td>".$FotoUser."</td><td>".$arItem['LOGIN']."</td><td>".$arItem['NAME']."</td></tr>";
}
//print_r ($rsUsers); - Раскомментировать, что бы увидеть все доступные поля
?>
</table>
</html><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>